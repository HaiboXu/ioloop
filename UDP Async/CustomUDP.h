//
// Created by xuhaibo on 2021/8/31.
//

#ifndef CUSTOMUDP_CUSTOMUDP_H
#define CUSTOMUDP_CUSTOMUDP_H

#include <thread>
#include <chrono>
#include <iostream>
#include <functional>
#include <string.h>
#include <mutex>
#include <unistd.h>
#ifdef __linux__
#include <netinet/in.h>
#include <arpa/inet.h>
#elif defined(_WIN32)
#include <WinSock2.h>
#include <windows.h>
#include <ws2tcpip.h>
//#pragma comment(lib,"WS2_32.lib")
#endif

class CustomUDP {
    typedef std::function<void(const std::string &)> CALLFunction;
#define MAXBUFFER           1500

public:
    enum {
        TIMEOUT = 1,
        CLIENT = 10,
        SERVER = 11
    };
    CustomUDP() ;

    ~CustomUDP();

    CustomUDP &operator=(const CustomUDP &ct);   //> 重载赋值操作符

    bool Connect(CALLFunction param_callfunction, const char* param_ref_ipaddress, int param_port, int param_type,
                 int param_timeout = 1);                    //>

    bool Connect(const std::string& param_ref_ip, int param_port, int param_type,
                 int param_timeout = 1);                    //>

    int Start();                                            //>

    bool Stop();                                            //> 关闭套接字
    //>
    const std::string TimeStamp();

    const int Recvice();

    const int SendData(const char *Buffer, int nSize);

    static void* RecviceThread(void *param_handler);

public:
    int cls_threadstatus;           //> 线程是否继续运行
#ifdef __linux__
    int cls_sockfd;                 //> UDP连接的套接字
#elif defined(_WIN32)
    SOCKET cls_sockfd;
#endif
    struct timeval cls_timeout;     //> UDP查询超时时间
    char cls_buffer[MAXBUFFER];      //> UDP 接收数据的缓存
private:
    CALLFunction clsReadfunction;   //> UDP接收消息后回调函数
    std::thread cls_thread;

    struct sockaddr_in cls_addr_sr; //> UDP收发地址
    int cls_port;                   //> UDP连接的端口号


    std::string cls_ip;             //> UDP连接的IP地址

    std::mutex cls_mutex;            //> UDP 异步读写使用互斥锁

};

void RecviceThread(void *param_handler);

#endif //CUSTOMUDP_CUSTOMUDP_H
