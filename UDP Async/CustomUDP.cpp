//
// Created by xuhaibo on 2021/8/31.
//

#include "CustomUDP.h"


void* CustomUDP::RecviceThread(void *param_handler)
{
    CustomUDP* pHandler = static_cast<CustomUDP*>(param_handler);
    try {
        /****************************> 开始循环读取数据 <****************************/
        int local_status = 0;                                                                                           //> 接收select状态
        fd_set local_fd;                                                                                                //> 读写状态
        pHandler->SendData("hello world", 11);

        while (pHandler->cls_threadstatus) {
            FD_ZERO(&local_fd);                                                                              //> 把可读文件描述符的集合清空
            FD_SET(0, &local_fd);                                                                            //> 把标准输入的文件描述符加入到集合中
            FD_SET(pHandler->cls_sockfd, &local_fd);                                                                       //> 把当前连接的文件描述符加入到集合中

            int reg_maxfd = pHandler->cls_sockfd + 1;                                                                             //> 文件描述符集合中最大的文件描述符

            switch (select(reg_maxfd, &local_fd, NULL, NULL, &(pHandler->cls_timeout))) {
            case 0:
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                continue;
            case -1:
                throw ("\"select(reg_maxfd, &local_fd, NULL, NULL, &cls_timeout) == -1\"");
            default:
                if (FD_ISSET(pHandler->cls_sockfd, &local_fd)) {                                                               //> 发来数据将会触发进入循环
                    int reg_size = pHandler->Recvice();
                    if (reg_size <= 0)
                        throw ("服务端连接断开！");
                    else {
                        std::string data = std::string(pHandler->cls_buffer, reg_size);
                        std::clog << data << std::endl;
    //                    clsReadfunction(std::string(pHandler->cls_buffer, reg_size));
                    }
                }

            }
        }
    }
    catch (...) {
        std::cerr << pHandler->TimeStamp() << errno << std::endl;
        close(pHandler->cls_sockfd);
    }
    std::clog<<pHandler->TimeStamp()<<"Thread Quit!"<<std::endl;
}

CustomUDP::CustomUDP() : cls_sockfd(-1),cls_threadstatus(false) {
}

CustomUDP::~CustomUDP() {
    Stop();
}

CustomUDP &CustomUDP::operator=(const CustomUDP &ct) {
    return *this;
}

const std::string CustomUDP::TimeStamp() {
    auto tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    struct tm *ptm = localtime(&tt);
    char date[60] = {0};
    sprintf(date, "[%04d-%02d-%02d %02d:%02d:%02d]",
            (int) ptm->tm_year + 1900, (int) ptm->tm_mon + 1, (int) ptm->tm_mday,
            (int) ptm->tm_hour, (int) ptm->tm_min, (int) ptm->tm_sec);
    return std::string(date);
}

const int CustomUDP::Recvice() {
    std::lock_guard<std::mutex> local_lock(cls_mutex);
    int addr_size = sizeof(cls_addr_sr);
//    int size = recvfrom(cls_sockfd, cls_buffer, MAXBUFFER, MSG_DONTWAIT, (struct sockaddr *) &cls_addr_sr,
//                        reinterpret_cast<socklen_t *>(&addr_size));
    int size = recvfrom(cls_sockfd, cls_buffer, MAXBUFFER, 0, (struct sockaddr *) &cls_addr_sr,
                        reinterpret_cast<socklen_t *>(&addr_size));
    return size;
}

const int CustomUDP::SendData(const char *param_data, int param_size) {
    std::lock_guard<std::mutex> local_lock(cls_mutex);
    return sendto(cls_sockfd, param_data, param_size, 0, (struct sockaddr *) &cls_addr_sr, sizeof(cls_addr_sr));
}

bool
CustomUDP::Connect(CALLFunction param_callfunction, const char *param_ref_ipaddress, int param_port, int param_type,
                   int param_timeout) {
    clsReadfunction = param_callfunction;
#ifdef defined(__linux__)

#elif defined(_WIN32)
    //初始化DLL
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif
    cls_ip = param_ref_ipaddress;
    cls_port = param_port;
    cls_timeout.tv_sec = 0;
    cls_timeout.tv_usec = param_timeout;

    if ((cls_sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        std::cerr << TimeStamp() << "\"socket(AF_INET, SOCK_DGRAM, 0))\"" << errno << std::endl;
        return false;
    }
    memset(&cls_addr_sr, 0, sizeof(cls_addr_sr));
    cls_addr_sr.sin_family = AF_INET;
    cls_addr_sr.sin_addr.s_addr = inet_addr(cls_ip.c_str());
    cls_addr_sr.sin_port = htons(cls_port);

//    setsockopt(cls_sockfd, SOL_SOCKET,SO_RCVTIMEO, (const char*)&cls_timeout,sizeof(struct timeval));
//    if(bind(cls_sockfd, reinterpret_cast<struct sockaddr*>(&cls_addr_sr), sizeof(struct sockaddr_in) ) == -1)
//    {
//        std::cerr<<TimeStamp()<<"Failed to bind socket on port "<<cls_port<<errno<<std::endl;
//        return -1;
//    }
    return true;
}

bool CustomUDP::Connect(const std::string& param_ref_ip,int param_port, int param_type, int param_timeout) {
    cls_port = param_port;
    cls_timeout.tv_sec = 0;
    cls_timeout.tv_usec = param_timeout;

    if ((cls_sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        std::cerr << TimeStamp() << "\"socket(AF_INET, SOCK_DGRAM, 0))\"" << errno << std::endl;
        return false;
    }
    memset(&cls_addr_sr, 0, sizeof(cls_addr_sr));
    cls_addr_sr.sin_family = AF_INET;
    cls_addr_sr.sin_port = htons(cls_port);
    if(param_type == CLIENT)
        cls_addr_sr.sin_addr.s_addr = inet_addr("172.30.193.73");
    if(param_type == SERVER)
    {
        cls_addr_sr.sin_addr.s_addr = htonl(
                INADDR_ANY);                                                            //>自动获取IP地址.INADDR_ANY表示不管是哪个网卡接收到数据，只要目的端口是SERV_PORT，就会被该应用程序接收到
//        setsockopt(cls_sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char *) &cls_timeout, sizeof(struct timeval));
        int result_code = bind(cls_sockfd, (struct sockaddr *)(&cls_addr_sr), sizeof(struct sockaddr_in));
        if (result_code < 0) {
            std::cerr << TimeStamp() << "Failed to bind socket on port " << cls_port << errno <<" [bind]:"<< result_code<< std::endl;
            return false;
        }
    }
    return true;
}

//>
int CustomUDP::Start() {
    cls_threadstatus = true;
    cls_thread = std::thread(RecviceThread,this);
}

//> 关闭套接字
bool CustomUDP::Stop() {
    if(cls_thread.joinable())
    {
        cls_threadstatus = false;
        cls_thread.join();
        return true;
    }

//    if (cls_sockfd == -1)
//        return false;
//    if (close(cls_sockfd))
//        return true;
//    else {
//        std::cerr << TimeStamp() << "\"close(cls_sockfd) faild!\" " << strerror(errno) << std::endl;
//        return false;
//    }
}
